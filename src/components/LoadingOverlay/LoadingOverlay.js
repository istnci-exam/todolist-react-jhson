import React from 'react';
import Loader from 'react-loader-spinner';
import "./LoadingOverlay.css";

const LoadingOverlay = (props) => {
    return(
    <div className="loading-overlay"> 

        <Loader type={"Oval"} width={30} height={30}/>

    </div>
    );
};

export default LoadingOverlay;