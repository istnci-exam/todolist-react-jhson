import React, { useEffect, useState } from "react";
import "./App.css";
import axios from "axios";
import moment from "moment";
import Moment from "react-moment";
import LoadingOverlay from "./components/LoadingOverlay/LoadingOverlay";
import Calendar from "react-calendar";

//Host setting
//mobile
//const host = "https://7f962632a979.ngrok.io";
//local
//const host = "http://localhost:5000";
//BTP
const host = "https://node-todolist-jhson.cfapps.us10.hana.ondemand.com";
let askId = prompt("ID를 입력하세요", "2576112");
function App() {
  const [index, setIndex] = useState(-1);
  const [modify, setModify] = useState(false);
  const [loading, setLoading] = useState(true);
  const [open, toggleOpen] = useState(false);
  const [todolist, setTodolist] = useState({ items: [] });
  const [todoCount, setTodoCount] = useState(0);
  const [openCalendar, setOpenCalendar] = useState(false);

  const updateTodolist = () => {
    const options = {
      url: `${host}/api/v1/todolist`,
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      data: todolist,
    };
    return axios(options);
  };

  const getTodolist = () => {
    const options = {
      url: `${host}/api/v1/todolist`,
      method: "GET",
      headers: {
        //user_id : "2576112",
        user_id: askId,
      },
    };
    return axios(options);
  };

  const handleDone = (e) => {
    //const id = e.target.id.split('-').reverse()[0]; //index가져오는 방법
    const id = e.currentTarget.id.split("-").reverse()[0];
    // const copydata = todolist; //*shallow copy 주소까지 카피
    // const copydata = JSON.parse(JSON.stringify(todolist)); //*deep copy (ES5)
    // //*deep copy (ES6) ... = spread operator : 새로운 객체로 복사하는 방법
    const copydata = { ...todolist };

    copydata.items[id].todo_done = !todolist.items[id].todo_done;
    copydata.items[id].modifiedAt = moment().format("YYYY-MM-DD");
    //todolist.items[id].todo_done = !todolist.items[id].todo_done;
    //setTOdolist(todolist); 안하는 이유 : 직접 접근 하지않고 반드시 상태값함수를 통해서 변경해야한다
    setTodolist(copydata);

    setLoading(true);
    updateTodolist(copydata)
      .then((response) => {
        //처리됐는지 안됐는지(결과내용은 아니고) > 처리됐으면 적용
        if (response.data) setTodolist(response.data);
        setTodoCount(count(response.data.items));
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
      });

      
  };
  const handleEdit = (e) => {
    setModify(true);
    const id = e.currentTarget.id.split("-").reverse()[0];
    todolist.items[id].edit = false; 
    todolist.items[id].edit = true; 
  };

  const handleHighlight = (e) => {
    setLoading(true);
    const id = e.currentTarget.id.split("-").reverse()[0];
    const copydata = { ...todolist };

    copydata.items[id].highlight = !todolist.items[id].highlight;
    //수정일자 안 적음
    setTodolist(copydata);
    updateTodolist(copydata)
      .then((response) => {
        if (response.data) setTodolist(response.data);
        setTodoCount(count(response.data.items));
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
      });
  };

  const handleDelete = (e) => {
    const idx = e.currentTarget.id.split("-").reverse()[0];
    const copydata = { ...todolist };
    // const copydata = JSON.parse(JSON.stringify(todolist));
   

    copydata.items.splice(idx, 1); //id 부터 1개 제거 = id제거
    setTodolist(copydata);



    setLoading(true);
    updateTodolist(copydata)
      .then((response) => {
        if (response.data) {
          setTodolist(response.data);
          setTodoCount(count(response.data.items));
          // setTodoCount(response.data.items.length);
          setLoading(false);
        }
      })
      .catch((error) => {
        setLoading(false);
      });
  };

  moment.locale("ko", {
    weekdays: [
      "일요일",
      "월요일",
      "화요일",
      "수요일",
      "목요일",
      "금요일",
      "토요일",
    ],
    weekdaysShort: ["일", "월", "화", "수", "목", "금", "토"],
  });

  useEffect(
    () => {
      getTodolist()
        .then((response) => {
          if (response.data) {
            setTodolist(response.data);
            setTodoCount(count(response.data.items));
            //setTodoCount(response.data.items.length);
            setLoading(false);
          }
        })
        .catch((error) => {
          setLoading(false);
        });
    },[] //의존성 배열
  );

  const count = (items) => {
    let i = 0;
    items.map((item) => {
      !item.todo_done && i++; //= if (!items[itemIdx].todo_done) {i ++}
      return i;
    });
    return i;
  };

  const sendMessage = (user_id) => {
    const options = {
      url: `${host}/api/v1/todolist/message`,
      method: "POST",
      data: {
        user_id: user_id,
      },
    };
    return axios(options);
  };
  const handleInputChange = (e) => {
    const idx = e.currentTarget.id.split("-").reverse()[0];
    const originData = todolist.items[idx].todo_title;
    
    if (originData !== e.currentTarget.value) {
      //setModify(true);
      //저장로직
      const copydata = { ...todolist };
      copydata.items[idx].todo_title = e.currentTarget.value;
      copydata.items[idx].modifiedAt = moment().format("YYYY-MM-DD");
      setTodolist(copydata);
      setLoading(true);
      
      updateTodolist(copydata)
        .then((response) => {
          if (response.data) setTodolist(response.data);
          setTodoCount(count(response.data.items));
          setLoading(false);
          setModify(false);
          todolist.items[idx].edit = false;
        })
        .catch((error) => {
          setLoading(false);
          setModify(false);
          todolist.items[idx].edit = false;
        });
    } else {
      setModify(false);
      todolist.items[idx].edit = false;
    }
  };

  const handleInputSave = (e) => {
    const todoText = e.target.value;
    if (todoText){
    const copydata = { ...todolist };
    const item = {
      id: "",
      todo_title: todoText,
      todo_done: false,
      createdAt: moment().format("YYYY-MM-DD HH:mm:ss"),
      modifiedAt: moment().format("YYYY-MM-DD"),
    };
    setLoading(true);
    sendMessage(copydata.user_id)
      .then((response) => {
        copydata.items.push(item);
        updateTodolist(copydata).then((response) => {
          if (response.data) {
            setTodolist(response.data);
            setTodoCount(count(response.data.items));
            //setTodoCount(response.data.items.length);
            e.target.value = "";
            toggleOpen(false);
            setLoading(false);
          }
        });
      })
      .catch((error) => {
        setLoading(false);
      });
  }else{
    toggleOpen(!open);
  }};
  const handleOpen = (e) => {
  
      toggleOpen(!open);
    
  };
  const handleDday = (e) => {
    //e 가 클릭한 날짜
    const day = moment(e);
    const today = moment(new Date());
    const dayDiff = day.diff(today, "days");
    

    //저장로직
    if (index >= 0) {
      const idx = index; //클릭한 idx
      const copydata = { ...todolist };
      copydata.items[idx].dDay = dayDiff;
      copydata.items[idx].modifiedAt = moment().format("YYYY-MM-DD");
      setTodolist(copydata);
      setLoading(true);

      updateTodolist(copydata)
        .then((response) => {
          if (response.data) setTodolist(response.data);
          setTodoCount(count(response.data.items));
          setLoading(false);
          setModify(false);
        })
        .catch((error) => {
          setLoading(false);
          setModify(false);
        });
      //달력닫기
      setOpenCalendar(false);
      //index설정
      setIndex(-1);
      todolist.items[idx].edit = false;
    }
  };

  const setCalendar = (e) => {
    const idx = e.currentTarget.id.split("-").reverse()[0];
    todolist.items.map((item, itemIdx) => {
      if(item.edit){
        if (itemIdx !== idx){
          item.edit = false;
        }
      }return item;
    });
    todolist.items[idx].edit = !openCalendar;
    setOpenCalendar(!openCalendar);
    setIndex(idx);
  };
  const handleKeyPress = (e) => {
    if (e.key === "Enter" && e.target.value) {
      const todoText = e.target.value;
      const copydata = { ...todolist };
      const item = {
        id: "",
        todo_title: todoText,
        todo_done: false,
        createdAt: moment().format("YYYY-MM-DD HH:mm:ss"),
        modifiedAt: moment().format("YYYY-MM-DD"),
      };
      setLoading(true);
      sendMessage(copydata.user_id)
        .then((response) => {
          copydata.items.push(item);
          updateTodolist(copydata).then((response) => {
            if (response.data) {
              setTodolist(response.data);
              setTodoCount(count(response.data.items));
              //setTodoCount(response.data.items.length);
              e.target.value = "";
              toggleOpen(false);
              setLoading(false);
            }
          });
        })
        .catch((error) => {
          setLoading(false);
        });
    }
  };
  // debugger;
  const handleCloseApp = (e) => {
    window.location.href = 'app://close';
  };     
  //todolist 가 변경됐을떄만 비교하기 로직
  // useEffect(() => {
  //   if (!todolist.items.length) return;
  //   const copyList = {...todolist};
  //   // copyList.items.sort((a, b) => {
  //   //   return a.modifiedAt > b.modifiedAt ? 1 : -1
  //   // });
  //    const sortList = copyList.items.sort( (a, b) => {

  //      return a.todo_done > b.todo_done ? 1 : -1 });
  //     copyList.items = sortList;
  //    setTodolist(copyList);
  // },[])

  return (
    <div className="App">
     <div className="app-close" onClick={handleCloseApp}>
      <div className="fas fa-times"></div>
      </div> 
      {loading && <LoadingOverlay />}
      <div className="todolist-date">
        <Moment format="YYYY년 MM월 DD일">{new Date()}</Moment>
      </div>
      <div className="todolist-secondline">
        <div className="todolist-dayofweek">
          <Moment format="dddd">{new Date()}</Moment>
        </div>
        <div className="todolist-askId">{`ID : ${askId}`}</div>
      </div>
      <div className="todolist-overview">{`할일  ${todoCount}개 남음`}</div>

      <div className="todolist-items">
        {todolist.items.map((item, itemIdx) => {
          return (
            <div className="todolist-item">
              <div className="todolist-item-content" key={itemIdx}>
                <div
                  id={`item-${itemIdx}`}
                  className={
                    item.todo_done ? "item-check item-done" : "item-check"
                  }
                  onClick={handleDone}
                >
                  {item.todo_done && <i class="fas fa-check"></i>}{" "}
                </div>
                <div id={`text-${itemIdx}`} onClick={handleEdit}>
                  { modify && item.edit ? 
                  <input  className={
                    item.todo_done
                      ? "item-input text-done"
                      : "item-input" && item.highlight
                      ? "item-input text-highlight"
                      : "item-input"
                  } onBlur={handleInputChange}
                    defaultValue={item.todo_title}
                    autoFocus = "true"
                    type="text"
                    id={`input-${itemIdx}`}/> :
                    <div  className={
                      item.todo_done
                        ? "item-text text-done"
                        : "item-text" && 
                        item.highlight
                        ? "item-text text-highlight"
                        : "item-text"
                    }
                    
                  >{item.todo_title}</div>

                  } 
                </div>
                <div
                  id={`calendar-${itemIdx}`}
                  className="item-calendar"
                  onClick={setCalendar}
                >
                  <i class="far fa-calendar-alt"></i>
                </div>
               
                <div
                  id={`highlight-${itemIdx}`}
                  className="item-highlight"
                  onClick={handleHighlight}
                >
                  <i class="fas fa-highlighter"></i>
                </div>
                <div
                  id={`delete-${itemIdx}`}
                  className="item-delete"
                  onClick={handleDelete}
                >
                  <i class="fas fa-trash"></i>
                </div>
              </div>
              <div className="item-time">
                <div className="item-Dday">{item.dDay ? `D-${item.dDay}` : "today"}</div>
                <div className="item-datetime">{item.modifiedAt}</div>
              </div>
              
                  {openCalendar && item.edit && (
                     <Calendar className="calendar" minDate={new Date()} onChange={handleDday} />
                  )}
               
            </div>
            
          );
        })}
      </div>

      {open && (
        <div className="bottom-sheet">
          <input
            placeholder="새로운 할 일을 입력하세요"
            onKeyPress={handleKeyPress}
            onBlur={handleInputSave}
          />
        </div>
      )}
      <div className="circle-button" onClick={handleOpen}>
        <i className="fas fa-plus"></i>
      </div>
    </div>
  );
}

export default App;
